﻿using StatCounter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StatCounter.DataPackages
{
    public class VehicleForecastPackage : PackagesBase
    {
        public string VehicleId { get; set; }
        public int RouteId { get; set; }

        public VehicleForecastPackage(Message message, int firstValuePosition = 1) : base(message)
        {
            var values = _Message.StrArray.Skip(firstValuePosition).ToList();
            VehicleId = values.Skip(3).FirstOrDefault();
            IsCorrect = int.TryParse(values.Skip(4).FirstOrDefault(), out int rId);

            if (IsCorrect)
                RouteId = rId;
        }        

        public override int GetHashCode()
        {
            return $"{UserId} {VehicleId} {RouteId}".GetHashCode();
        }
    }
}
