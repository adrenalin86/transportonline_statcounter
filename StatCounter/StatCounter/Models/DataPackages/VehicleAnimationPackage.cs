﻿using StatCounter.DataPackages;
using System.Linq;

namespace StatCounter.Models.DataPackages
{
    public class VehicleAnimationPackage : PackagesBase
    {
        public int RouteId { get; set; }        

        public VehicleAnimationPackage(Message message) : base(message)
        {
            RouteId = 0;
            IsCorrect = true;
        }

        public override int GetHashCode()
        {
            return $"{UserId}{RouteId}{CityId}".GetHashCode();
        }

    }
}
