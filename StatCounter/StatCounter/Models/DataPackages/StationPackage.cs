﻿using StatCounter.Models;
using StatCounter.Services;
using System;
using System.Linq;

namespace StatCounter.DataPackages
{
    public class StationPackage : PackagesBase
    {
        public int StationId { get; set; }

        public StationPackage(Message message, int firstValuePosition = 1) : base(message, firstValuePosition)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            var values = _Message.StrArray.Skip(firstValuePosition).ToList();
            IsCorrect = int.TryParse(values.Skip(3).FirstOrDefault(), out int stId);
            
            if (IsCorrect)
                StationId = stId;         
        }

        public override int GetHashCode()
        {
            return $"{UserId}{StationId}{CityId}".GetHashCode();
        }

    }
}
