﻿using StatCounter.Models;
using StatCounter.Services;
using System;
using System.Linq;

namespace StatCounter.DataPackages
{
    public abstract class PackagesBase : IPackage
    {
        protected static readonly int DEFAULT_SESSION_DURATION = 180;

        protected internal int _TimeStamp;
        protected internal Message _Message;
        
        public int TimeStamp { get { return _TimeStamp; } }        
        public string UserId { get; set; }
        public string CityId { get; set; }
        public string Message { get { return _Message.TextData ?? String.Empty; } }
        public bool IsCorrect { get; set; }
        public int ArrTimeN { get; set; }
        public int ArrTimeF { get; set; }

        public PackagesBase(Message message, int firstValuePosition = 1)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            _Message = message;
            IsCorrect = true;
            var values = _Message.StrArray.Skip(firstValuePosition).ToList();
            IsCorrect = int.TryParse(values.FirstOrDefault(), out _TimeStamp);
            UserId = values.Skip(1).FirstOrDefault();
            CityId = values.Skip(2).FirstOrDefault();
            ArrTimeN = ArrTimeF = UnixTimeStampConverter.GetTimeStamp();
        }

        public virtual bool IsComplete(int time)
        {
            return time - ArrTimeN >= GetSessionDuration();
        }

        public virtual int GetSessionDuration()
        {
            return DEFAULT_SESSION_DURATION;
        }

    }
}
