﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace StatCounter.Models
{
    public class Message
    {
        private static readonly Encoding _Encoding = Encoding.GetEncoding(1251);
        public string Mark { get; private set; }
        public string[] StrArray { get; private set; }        
        public string TextData { get; private set; }
        public int Length => StrArray?.Length ?? 0;

        public Message(byte[] data)
        {
            data = data ?? new byte[] { };
            TextData = new String(_Encoding.GetChars(data.Skip(1).ToArray()));
            StrArray = TextData.Split(';');
            Mark = StrArray.FirstOrDefault() ?? String.Empty;
            // в случае, если метки нет в сообщении, вместо нее в первом слове будет содержаться timestamp.
            // для такого сообщения указываем метку 'none'
            if (int.TryParse(Mark, out int num)) Mark = "none";
        }

        public override string ToString()
        {
            return TextData;
        }
    }
}