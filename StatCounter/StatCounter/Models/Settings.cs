﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace StatCounter.Models
{
    public class Settings
    {
        private readonly IConfiguration configuration;

        public Settings(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string Server => configuration.GetValue<string>("DBServer");
        public string DbPort => configuration.GetValue<string>("DBPort");
        public string DataBase => configuration.GetValue<string>("DataBase");
        public string Uid => configuration.GetValue<string>("Uid");
        public string Password => configuration.GetValue<string>("Password");
        public int Port => configuration.GetValue<int>("Port");
        public string[] Cities => configuration.GetSection("Cities").AsEnumerable().Select(x=>x.Value).Where(x=>x!=null).ToArray();
    }
}
