﻿using NLog;
using System;
using System.Collections.Generic;
using System.Text;

namespace StatCounter
{
    public class NLogger
    {
        private Logger _Logger;
        private DateTime _LastErrorTimeUtc;
        private string _LastErrorMessage;

        public bool T_Enabled => _Logger?.IsTraceEnabled ?? false;
        public bool D_Enabled => _Logger?.IsDebugEnabled ?? false;
        public bool I_Enabled => _Logger?.IsInfoEnabled ?? false;
        public bool W_Enabled => _Logger?.IsWarnEnabled ?? false;

        public NLogger(string name)
        {
            _Logger = LogManager.GetLogger(name);
        }

        private void Log(LogLevel level, string message, Exception ex = null)
        {
            LogEventInfo theEvent = new LogEventInfo(level, "", System.Globalization.CultureInfo.InvariantCulture, message, null, ex);
            _Logger.Log(theEvent);
        }

        public void T(string message, Exception ex = null)
        {
            if (T_Enabled)
                Log(LogLevel.Trace, message, ex);
        }

        public void D(string message, Exception ex = null)
        {
            if (D_Enabled)
                Log(LogLevel.Debug, message, ex);
        }

        public void I(string message, Exception ex = null)
        {
            if (I_Enabled)
                Log(LogLevel.Info, message, ex);
        }

        public void W(string message, Exception ex = null)
        {
            if (W_Enabled)
                Log(LogLevel.Warn, message, ex);
        }

        public void E(string message, Exception ex = null)
        {
            _LastErrorMessage = message;
            _LastErrorTimeUtc = DateTime.UtcNow;
            Log(LogLevel.Error, message, ex);
        }

        public void F(string message, Exception ex = null)
        {
            _LastErrorMessage = message;
            _LastErrorTimeUtc = DateTime.UtcNow;
            Log(LogLevel.Fatal, message, ex);
        }
    }
}
