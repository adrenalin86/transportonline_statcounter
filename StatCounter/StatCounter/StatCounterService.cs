﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using StatCounter.Models;
using StatCounter.Services;
using StatCounter.Processors;

namespace StatCounter
{
    public class StatCounterService : IHostedService, IDisposable
    {
        private readonly NLogger _Logger;
        private Settings _Settings;
        IDictionary<string, IProcessor> _Processors;
        private UDPListener _Listener;
        private MySqlAdapter _DbAdapter;
        private CancellationToken _CancelToken;

        public StatCounterService(Settings settings)
        {
            _Logger = new NLogger(GetType().Name);
            _Logger.I("Starting service...");
            _Settings = settings;
            _DbAdapter = new MySqlAdapter(_Settings);
            
            var stationProcessor = new StationProcessor(_Settings) { DbAdapter = _DbAdapter };
            var vehicleProcessor = new VehicleAnimationProcessor( _Settings) { DbAdapter = _DbAdapter };
            var vehForecastProc = new VehicleForecastProcessor( _Settings) { DbAdapter = _DbAdapter };

            _Processors = new Dictionary<string, IProcessor>()
            {
                [stationProcessor.GetMark()] = stationProcessor,
                [vehicleProcessor.GetMark()] = vehicleProcessor,
                [vehForecastProc.GetMark()] = vehForecastProc,
                ["none"] = stationProcessor
            };

            _Listener = new UDPListener(_Processors, _Settings);
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            _Logger.I("Service started");
        }

        public void Dispose()
        {
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _Logger.D("Worker running");
            _CancelToken = stoppingToken;            
            _DbAdapter.Initialize(stoppingToken);
            _Processors.Values.ToList().ForEach(p => p.Start());
            _Listener.Start(stoppingToken);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _Listener?.Stop();
            _Processors.Values.ToList().ForEach(p => p.Stop());
            return Task.CompletedTask;
        }
    }
}
