﻿using System;
using StatCounter.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace StatCounter.Services
{
    public class UDPListener
    {
        private int _ListenPort;
        private NLogger _Logger;        
        private IDictionary<string, IProcessor> _Processors;
        private List<byte[]> _ProcessingList;
        private Thread _ListeningThread;
        private Thread _ProcessingThread;
        private CancellationToken _CancelToken;

        public UDPListener(IDictionary<string, IProcessor> processors, Settings settings)
        {
            _Logger = new NLogger(GetType().Name);
            _ListenPort = settings.Port;
            _Processors = processors;
            _ProcessingList = new List<byte[]>();
            _Logger.D($"UDPListener Created");
        }

        public void Start(CancellationToken stoppingToken)
        {
            _CancelToken = stoppingToken;            
            _ListeningThread = new Thread(new ThreadStart(ListeningRoutine)) { IsBackground = true };
            _ListeningThread.Start();
            _ProcessingThread = new Thread(new ThreadStart(ProcessingRoutine)) { IsBackground = true };
            _ProcessingThread.Start();
            _Logger.D($"UDPListener Started");
        }

        public void Stop()
        {
            _ListeningThread = null;
            _ProcessingThread = null;
            lock (_ProcessingList)
            {
                _ProcessingList.Clear();
            }            
        }

        private void ListeningRoutine()
        {
            var listener = new UdpClient(_ListenPort);
            var endPoint = new IPEndPoint(IPAddress.Any, _ListenPort);

            try
            {               
                while (!_CancelToken.IsCancellationRequested && _ListeningThread != null)
                {
                    byte[] bytes = listener.Receive(ref endPoint);
                    lock (_ProcessingList)
                    {
                        _ProcessingList.Add(bytes);
                    }
                }
            }
            catch (SocketException ex)
            {
                _Logger.E($"UDPListener Socket Exception", ex);
            }
            catch (Exception ex)
            {
                _Logger.E("UDPListener Exception", ex);
            }
            finally
            { 
                listener.Close();
            }
        }

        private void ProcessingRoutine()
        {
            while (!_CancelToken.IsCancellationRequested && _ProcessingThread != null)
            {
                try
                {
                    List<byte[]> localList = null;

                    lock (_ProcessingList)
                    {
                        if (_ProcessingList.Count == 0)
                            continue;

                        localList = _ProcessingList;
                        _ProcessingList = new List<byte[]>();
                    }

                    localList.ForEach(x =>
                    {
                        IProcessor processor = null;
                        var mess = new Message(x);
                        var time = DateTime.Now;
                        if (_Logger.D_Enabled)
                            _Logger.D($"UDPListener accepted package: {mess}");

                        if (!_Processors.TryGetValue(mess.Mark, out processor))
                        {
                            if (_Logger.D_Enabled)
                                _Logger.D($"UDPListener discarded incorrect package with data {mess}");
                            return;
                        }
                        if (!processor.IsLengthCorrect(mess))
                        {
                            if (_Logger.D_Enabled)
                                _Logger.D($"UDPListener discarded wrong length package with data {mess}");
                            return;
                        }

                        processor.AddPackage(mess);
                    });
                }
                catch (Exception ex)
                {
                    _Logger.E($"ProcessingRoutine", ex);
                }
                finally
                {
                    Thread.Sleep(100);
                }
            }
        }
    }
}
