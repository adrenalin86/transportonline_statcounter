﻿using System;

namespace StatCounter.Services
{
    public static class UnixTimeStampConverter
    {
        private static readonly DateTime EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime Convert(int timeStamp)
        {
            DateTime result = EPOCH.AddSeconds(timeStamp);
            return result;
        }

        public static DateTime Convert(string str)
        {
            int timeStamp = 0;
            Int32.TryParse(str, out timeStamp);
            return Convert(timeStamp);
        }

        public static int GetTimeStamp()
        {
            return (int)(DateTime.UtcNow.Subtract(EPOCH)).TotalSeconds;
        }
    }
}
