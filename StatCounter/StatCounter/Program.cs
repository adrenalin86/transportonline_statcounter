using StatCounter.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace StatCounter
{
    public class Program
    {
    
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        static string CombineToPath(string s1, string s2)
        {
            return Path.Combine(new string[] { s1, s2 });
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(confBuilder =>
                {
                    var path = Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                    confBuilder.AddJsonFile(CombineToPath(path, "config.json"));
                    confBuilder.AddCommandLine(args);
                })
                .UseWindowsService()
                .UseSystemd()                
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton<Settings>();
                    services.AddHostedService<StatCounterService>();
                });
    }
}
