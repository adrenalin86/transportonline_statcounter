﻿using StatCounter.Models;
using System.Collections.Generic;

namespace StatCounter
{
    public interface IProcessor
    {
        string GetMark();
        void AddPackage(Message message);
        bool IsLengthCorrect(Message message);
        void Start();
        void Stop();
    }
}
