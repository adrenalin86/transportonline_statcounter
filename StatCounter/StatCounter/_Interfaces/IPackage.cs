﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatCounter
{
    public interface IPackage
    {
        int ArrTimeN { get; set; }
        int ArrTimeF { get; set; }
        bool IsComplete(int time);
        string CityId { get; set; }
    }
}
