﻿using StatCounter.Models;
using System;
using System.Collections.Generic;
using StatCounter.DataPackages;

namespace StatCounter.Processors
{
    public class VehicleForecastProcessor : ProcessorBase
    {
        private static readonly string MARK = "gvf";

        public VehicleForecastProcessor(Settings settings) : base(settings)
        {
        }

        public override string GetMark()
        {
            return MARK;
        }

        public override bool IsLengthCorrect(Message message)
        {
            return message?.StrArray?.Length == 6;
        }

        protected override IEnumerable<IPackage> CreatePackages(Message message)
        {
            try
            {
                return new List<IPackage>() { new VehicleForecastPackage(message) };
            }
            catch (Exception ex)
            {
                _Logger.E($"AddPackage: {ex.Message}", ex);
                return null;
            }
        }
    }
}
