﻿using System;
using System.Collections.Generic;
using System.Linq;
using StatCounter.DataPackages;
using StatCounter.Models;

namespace StatCounter.Processors
{
    public class StationProcessor : ProcessorBase
    {
        private static readonly string MARK = "gsf";
        protected internal readonly object queueOutLock = new object();

        public StationProcessor(Settings settings) : base(settings)
        {
        }

        public override string GetMark()
        {
            return MARK;
        }

        public override bool IsLengthCorrect(Message message)
        {
            if (message.Mark.Equals("none"))
                return message?.StrArray?.Length == _DefaultMessageArrayLength - 1;
            else
                return message?.StrArray?.Length == _DefaultMessageArrayLength;
        }

        protected override IEnumerable<IPackage> CreatePackages(Message message)
        {
            try
            {
                int firstValuePos = message.Mark.Equals("none") ? 0 : 1;
                return new List<IPackage>() { new StationPackage(message, firstValuePos) };
            }            
            catch (Exception ex)
            {
                _Logger.E($"AddPackage", ex);
                return null;
            }
        }

        


    }
}

