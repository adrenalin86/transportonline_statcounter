﻿using StatCounter.Models;
using StatCounter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace StatCounter.Processors
{
    public abstract class ProcessorBase : IProcessor
    {     
        private IDictionary<int, IPackage> _ActivePackages;
        protected internal readonly NLogger _Logger;     
        protected internal int _DefaultMessageArrayLength;       
        private string[] _Cities;
        private Thread _CheckingThread;
        public MySqlAdapter DbAdapter { get; set; }

        public ProcessorBase(Settings settings)
        {
            _Logger = new NLogger(GetType().Name);
            _Cities = settings.Cities;
            _ActivePackages = new Dictionary<int, IPackage>();
            _DefaultMessageArrayLength = 5;
        }

        public void Start()
        {           
            _CheckingThread = new Thread(new ThreadStart(CheckingRoutine)) { IsBackground = true };
            _CheckingThread.Start();
        }

        public void Stop()
        {
            _CheckingThread = null;
            _ActivePackages.Clear();
        }

        public abstract string GetMark();
        protected abstract IEnumerable<IPackage> CreatePackages(Message message);

        public void AddPackage(Message message)
        {
            var packages = CreatePackages(message).Where(p =>
            {
                #region Validation
                if (p == null)
                {
                    return false;
                }
                else if (_Cities.Contains(p.CityId))
                {
                    return true;
                }
                else
                {
                    /*
                     если строки CityId из пакета нет в массиве cities из конфигурации,
                     то такой пакет не записывается.
                    */
                    if (_Logger.D_Enabled) _Logger.D($"city is not specified in config:{p.CityId}");
                    return false;
                }
                #endregion
            })
            .ToList();

            lock (_ActivePackages)
            {

                packages.ForEach(p =>
                {
                    if (_ActivePackages.TryGetValue(p.GetHashCode(), out IPackage target))
                        target.ArrTimeF = p.ArrTimeN;
                    else
                        _ActivePackages.Add(p.GetHashCode(), p);
                });
            }
        }

        public virtual bool IsLengthCorrect(Message message)
        {
            return message?.StrArray?.Length == _DefaultMessageArrayLength;
        }

        public virtual void CheckingRoutine()
        {
            while (_CheckingThread != null)
            {
                try
                {
                    List<IPackage> toRecord = new List<IPackage>(); // готовые пакеты для записи
                    var time = UnixTimeStampConverter.GetTimeStamp();

                    lock (_ActivePackages)
                    {
                        Dictionary<int, IPackage> toReturn = new Dictionary<int, IPackage>(); // еще не готовы
                        foreach (var pack in _ActivePackages)
                        {
                            // для надежности
                            if (pack.Value == null)
                                continue;

                            if (pack.Value.IsComplete(time))
                            {
                                toRecord.Add(pack.Value);
                                continue;
                            }

                            toReturn.Add(pack.Key, pack.Value);
                        }                        
                        _ActivePackages = toReturn;
                    }

                    if (toRecord.Count > 0)
                    {
                        // уже после разблокировки _ActivePackages обрабатываем результат
                        DbAdapter?.AddPackages(toRecord);
                        if (_Logger.D_Enabled) _Logger.D($"Packages sent to record: count={toRecord.Count}, time={DateTime.Now}");
                    }
                }
                catch (Exception ex)
                {
                    _Logger.E("ExctractCompletePackages", ex);
                }
                finally
                {
                    Thread.Sleep(1000);
                }
            }
        }
    }
}

