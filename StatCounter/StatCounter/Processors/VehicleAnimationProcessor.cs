﻿using System;
using System.Collections.Generic;
using StatCounter.Models;
using StatCounter.Models.DataPackages;

namespace StatCounter.Processors
{
    class VehicleAnimationProcessor : ProcessorBase
    {
        private static readonly string MARK = "gva";
        protected internal readonly object queueOutLock = new object();

        public VehicleAnimationProcessor(Settings settings) : base(settings)
        {
        }

        public override string GetMark()
        {
            return MARK;
        }

        protected override IEnumerable<IPackage> CreatePackages(Message message)
        {
            try
            {
                return new List<IPackage>() { new VehicleAnimationPackage(message) };
            }
            catch (Exception ex)
            {
                _Logger.E("AddPackage", ex);
                return null;
            }                     
        }

    }
}
