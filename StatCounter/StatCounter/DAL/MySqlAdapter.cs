﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Text;
using MySql.Data.MySqlClient;
using StatCounter.DataPackages;
using StatCounter.Models;
using StatCounter.Models.DataPackages;

namespace StatCounter
{
    public class MySqlAdapter
    {
        private static readonly int MAX_STRING_LENGTH = 100;
        private static int _MaxPackagesInInsert = 100;

        private string _ConnString;
        private bool _NeedReconnect;
        private string _Server;
        private string _Port;
        private string _Login;
        private string _Password;
        private List<string> _Cities;
        private NLogger _Logger;
        private MySqlConnection _MySqlConnection;        
        private Thread _RecordingThread;
        private List<IPackage> _RecordingList;
        private CancellationToken _CancelToken;

        public MySqlAdapter(Settings settings)
        {
            _Logger = new NLogger(GetType().Name);
            _Cities = settings.Cities?.ToList();
            _Server = settings.Server;
            _Port = settings.DbPort;
            _Login = settings.Uid;
            _Password = settings.Password;
            _ConnString = $"SERVER={_Server};PORT={_Port};UID={_Login};PASSWORD={_Password};";
            _NeedReconnect = true;
            _RecordingList = new List<IPackage>();
        }

        public void Initialize(CancellationToken token)
        {
            try
            {
                _CancelToken = token;

                if (_Logger.I_Enabled)
                {
                    _Logger.I($"ExecutingAssembly: path={Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName)}, time={DateTime.Now}");
                    _Logger.I("DB initializing. Connection string " + $"SERVER={_Server};PORT={_Port};UID***;PASSWORD=***; time={DateTime.Now}");
                }
                
                if (_NeedReconnect) Reconnect();

                _Cities.ForEach(c => 
                {
                    // создаем БД города
                    var query = $"CREATE DATABASE IF NOT EXISTS {c};";
                    if (ExecuteCommand(new MySqlCommand(query)))
                        if (_Logger.I_Enabled) 
                            _Logger.I($"Database created or already exist: name={c}");
                    else
                        _Logger.W($"Can not create database: name={c}");

                    // создаем таблицу gsf
                    var gsfCreate = $"CREATE TABLE IF NOT EXISTS {c}.`gsf` ("
                                     + @"`ID` int(11) NOT NULL AUTO_INCREMENT,
                                      `user_id` varchar(100) COLLATE cp1251_bin NOT NULL,
                                      `s_id` int(11) NOT NULL,
                                      `arr_timeN` int(11) NOT NULL,
                                      `arr_timeF` int(11) NOT NULL,
                                      PRIMARY KEY (`ID`),"
                                     + $" KEY `{c}_dt_N` (`s_id`,`arr_timeN`),"
                                     + $" KEY `{c}_dt_F` (`s_id`,`arr_timeF`),"
                                     + $" KEY `{c}_all` (`s_id`,`arr_timeF`,`arr_timeN`) "
                                     + ") ENGINE=InnoDB AUTO_INCREMENT=286996 DEFAULT CHARSET=cp1251 COLLATE=cp1251_bin;";

                    if (ExecuteCommand(new MySqlCommand(gsfCreate)))
                        if (_Logger.I_Enabled)
                            _Logger.I($"Table created or already exist: name={c}.gsf");
                    else
                        _Logger.W($"Can not create table: name={c}.gsf");

                    // создаем таблицу gva
                    string gvaCreate = $"CREATE TABLE IF NOT EXISTS {c}.`gva` ("
                                     + @"`ID` int(11) NOT NULL AUTO_INCREMENT,
                                      `user_id` varchar(100) COLLATE cp1251_bin NOT NULL,
                                      `r_id` int(11) NOT NULL,
                                      `arr_timeN` int(11) NOT NULL,
                                      `arr_timeF` int(11) NOT NULL,
                                      PRIMARY KEY (`ID`)
                                    ) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_bin;";

                    if (ExecuteCommand(new MySqlCommand(gvaCreate)))
                        if (_Logger.I_Enabled) 
                            _Logger.I($"Table created or already exist: name={c}.gva");
                    else
                        _Logger.W($"Can not create table: name={c}.gva");

                    // создаем таблицу gvf
                    string gvfCreate = $"CREATE TABLE IF NOT EXISTS {c}.`gvf` ("
                                     + @"`ID` int(11) NOT NULL AUTO_INCREMENT,
                                      `user_id` varchar(100) COLLATE cp1251_bin NOT NULL,
                                      `veh_id` varchar(100) COLLATE cp1251_bin NOT NULL,
                                      `r_id` int(11) NOT NULL,
                                      `arr_timeN` int(11) NOT NULL,
                                      `arr_timeF` int(11) NOT NULL,
                                      PRIMARY KEY (`ID`)
                                    ) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_bin;";

                    if (ExecuteCommand(new MySqlCommand(gvfCreate)))
                        if (_Logger.I_Enabled)
                            _Logger.I($"Table created or already exist: name={c}.gvf");
                    else
                        _Logger.W($"Can not create table: name={c}.gvf");

                });

                _RecordingThread = new Thread(new ThreadStart(RecordingRoutine)) { IsBackground = true, Priority = ThreadPriority.Highest };
                _RecordingThread.Start();
            }
            catch (Exception ex)
            {
                _Logger.E("DB connection can not create database", ex);
            }
        }

        private void Reconnect()
        {
            if (_MySqlConnection != null)
            {
                _MySqlConnection.Close();
                _MySqlConnection.Dispose();
                _Logger.D($"MySql connection closed");
            }
            _MySqlConnection = new MySqlConnection(_ConnString);
            _MySqlConnection.Open();

            if (_MySqlConnection.State != ConnectionState.Open)
            {
                throw new ApplicationException("Connection attempt failed");
            }

            _Logger.D($"new MySql connection created");
            _NeedReconnect = false;
        }

        private bool ExecuteCommand(MySqlCommand cmd)
        {
            try
            {
                if (_NeedReconnect) Reconnect();
                cmd.Connection = _MySqlConnection;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (ApplicationException ex)
            {
                _NeedReconnect = true;
                if (_Logger.I_Enabled) 
                    _Logger.I($"can not open connection to mysql server", ex);
            }
            catch (Exception ex)
            {
                _NeedReconnect = true;
                if (_Logger.I_Enabled)
                    _Logger.I($"can not execute mysql command '{cmd?.CommandText}'", ex);
            }

            return false;
        }

        public void AddPackages(IEnumerable<IPackage> packages)
        {
            lock (_RecordingList)
            {
                _RecordingList.AddRange(packages);
            }
        }

        private string CropString(string str)
        {
            return str.Substring(0, Math.Min(str.Length, MAX_STRING_LENGTH));
        }

        private void RecordingRoutine()
        {
            while (!_CancelToken.IsCancellationRequested && _RecordingThread != null)
            {
                try
                {
                    List<IPackage> localList = null;

                    lock (_RecordingList)
                    {
                        if (_RecordingList.Count == 0)
                            continue;

                        localList = _RecordingList;
                        _RecordingList = new List<IPackage>();
                    }

                    while (localList.Count > 0)
                    {
                        localList.Take(_MaxPackagesInInsert).GroupBy(p => p.CityId).ToList().ForEach(g =>
                        {
                            #region StationPackage                          
                            var gsfPacks = g.Where(p => p is StationPackage).Select(p => p as StationPackage).ToList();
                            if (gsfPacks.Count > 0)
                            {
                                var gsfInsert = new MySqlCommand();
                                var gsfQuery = new StringBuilder($"INSERT INTO {g.Key}.gsf(user_Id, s_Id, arr_timeN, arr_timeF) VALUES ");
                                int gsfRowNum = 0;
                                gsfPacks.ForEach(p =>
                                {
                                    if (gsfRowNum > 0) gsfQuery.Append(", ");
                                    gsfQuery.Append(String.Format("(@userId{0}, @sId{0}, @arrTimeN{0}, @arrTimeF{0})", gsfRowNum));
                                    gsfInsert.Parameters.Add($"@userId{gsfRowNum}", MySqlDbType.String).Value = CropString(p.UserId);
                                    gsfInsert.Parameters.Add($"@sId{gsfRowNum}", MySqlDbType.Int32).Value = p.StationId;
                                    gsfInsert.Parameters.Add($"@arrTimeN{gsfRowNum}", MySqlDbType.Int32).Value = p.ArrTimeN;
                                    gsfInsert.Parameters.Add($"@arrTimeF{gsfRowNum}", MySqlDbType.Int32).Value = p.ArrTimeF;
                                    gsfRowNum++;
                                });
                                gsfQuery.Append(";");
                                gsfInsert.CommandText = gsfQuery.ToString();

                                if (ExecuteCommand(gsfInsert))
                                    if (_Logger.D_Enabled)
                                        _Logger.D($"Packages saved: db={g.Key}.gsf, count={gsfPacks.Count}");
                                else
                                    if (_Logger.D_Enabled)
                                        _Logger.D($"Can not save packages: name={g.Key}.gsf, count={gsfPacks.Count}");
                            }                            
                            #endregion

                            #region VehicleAnimationPackage
                            var gvaPacks = g.Where(p => p is VehicleAnimationPackage).Select(p => p as VehicleAnimationPackage).ToList();
                            if (gvaPacks.Count > 0)
                            {
                                var gvaInsert = new MySqlCommand();
                                var gvaQuery = new StringBuilder($"INSERT INTO {g.Key}.gva(user_Id, r_Id, arr_timeN, arr_timeF) VALUES ");
                                int gvaRowNum = 0;
                                gvaPacks.ForEach(p =>
                                {
                                    if (gvaRowNum > 0) gvaQuery.Append(", ");
                                    gvaQuery.Append(String.Format("(@userId{0}, @rId{0}, @arrTimeN{0}, @arrTimeF{0})", gvaRowNum));
                                    gvaInsert.Parameters.Add($"@userId{gvaRowNum}", MySqlDbType.String).Value = CropString(p.UserId);
                                    gvaInsert.Parameters.Add($"@rId{gvaRowNum}", MySqlDbType.Int32).Value = p.RouteId;
                                    gvaInsert.Parameters.Add($"@arrTimeN{gvaRowNum}", MySqlDbType.Int32).Value = p.ArrTimeN;
                                    gvaInsert.Parameters.Add($"@arrTimeF{gvaRowNum}", MySqlDbType.Int32).Value = p.ArrTimeF;
                                    gvaRowNum++;
                                });
                                gvaQuery.Append(";");
                                gvaInsert.CommandText = gvaQuery.ToString();

                                if (ExecuteCommand(gvaInsert))
                                    if (_Logger.D_Enabled)
                                        _Logger.D($"Packages saved: db={g.Key}.gsf, count={gvaPacks.Count}");
                                else
                                    if (_Logger.I_Enabled)
                                        _Logger.I($"Can not save packages: name={g.Key}.gsf, count={gsfPacks.Count}");
                            }
                            #endregion

                            #region VehicleForecast
                            var gvfPacks = g.Where(p => p is VehicleForecastPackage).Select(p => p as VehicleForecastPackage).ToList();
                            if (gvfPacks.Count > 0)
                            {
                                var gvfInsert = new MySqlCommand();
                                var gvfQuery = new StringBuilder($"INSERT INTO {g.Key}.gvf(user_Id, veh_id, r_Id, arr_timeN, arr_timeF) VALUES ");
                                int gvfRowNum = 0;
                                gvfPacks.ForEach(p =>
                                {
                                    if (gvfRowNum > 0) gvfQuery.Append(", ");
                                    gvfQuery.Append(String.Format("(@userId{0}, @vehId{0}, @rId{0}, @arrTimeN{0}, @arrTimeF{0})", gvfRowNum));
                                    gvfInsert.Parameters.Add($"@userId{gvfRowNum}", MySqlDbType.String).Value = CropString(p.UserId);
                                    gvfInsert.Parameters.Add($"@vehId{gvfRowNum}", MySqlDbType.String).Value = CropString(p.VehicleId);
                                    gvfInsert.Parameters.Add($"@rId{gvfRowNum}", MySqlDbType.Int32).Value = p.RouteId;
                                    gvfInsert.Parameters.Add($"@arrTimeN{gvfRowNum}", MySqlDbType.Int32).Value = p.ArrTimeN;
                                    gvfInsert.Parameters.Add($"@arrTimeF{gvfRowNum}", MySqlDbType.Int32).Value = p.ArrTimeF;
                                    gvfRowNum++;
                                });
                                gvfQuery.Append(";");
                                gvfInsert.CommandText = gvfQuery.ToString();

                                if (ExecuteCommand(gvfInsert))
                                    if (_Logger.D_Enabled)
                                        _Logger.D($"Packages saved: db={g.Key}.gsf, count={gvfPacks.Count}");
                                else
                                    if (_Logger.I_Enabled)
                                        _Logger.I($"Can not save packages: name={g.Key}.gsf, count={gvfPacks.Count}");

                            }
                            #endregion
                        });

                        localList = localList.Skip(_MaxPackagesInInsert).ToList();
                    }                    
                }
                catch (Exception ex)
                {
                    _Logger.E("RecordingRoutine:", ex);
                }
                finally
                {
                    Thread.Sleep(100);
                }
            }
        }

    }
}
